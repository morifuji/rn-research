import React from 'react';
import { Image, StyleSheet, SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as Sharing from 'expo-sharing';

export default function App() {

  const [selectedImage, setSelectedImage] = React.useState<{ localUri:string }>({
    localUri: ""
  });

  let openImagePickerAsync = async () => {
    let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync()

    if (permissionResult.granted === false) {
      alert("permission required!")
      return
    }

    let pickerResult = await ImagePicker.launchImageLibraryAsync()
    if (pickerResult.cancelled) {
      return
    }
    console.log(pickerResult)

    setSelectedImage({ localUri: pickerResult.uri });
  }


  let expoImageSharingAsync = async () => {
    if (!(await Sharing.isAvailableAsync())) {
      alert("oh no. you can't sharing")
      return
    }


    Sharing.shareAsync(selectedImage.localUri)
  }

  if (selectedImage.localUri === "") {
    return <SafeAreaView style={{
    // flex: 1,
  }}>
      <TouchableOpacity 
    onPress={openImagePickerAsync}
    style={{backgroundColor: "blue"}}
    >
      <Text style={{ fontSize: 20, color: "red" }} >押してください！！！</Text>
      </TouchableOpacity>
    </SafeAreaView>
  }

  return (
    <SafeAreaView style={{
      // flex: 1,
    }}>
    <View >
      <Text>Changed by morifuhi.... Open up App.tsx to start working on your app!</Text>
      {/* <Image source={{
        uri: "https://media1.tenor.com/images/c7504b9fb03c95b3b5687d744687e11c/tenor.gif?itemid=7212866"
      }} style={{width: 305, height: 159}}/> */}
      <Image source={{ uri: selectedImage.localUri }} style={{width: 305, height: 159}} />

      <TouchableOpacity 
      onPress={expoImageSharingAsync}
      style={{backgroundColor: "blue"}}
      >
        <Text style={{ fontSize: 20, color: "red" }} >押してください！！！</Text>
        </TouchableOpacity>
    </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: '#fff',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
});
