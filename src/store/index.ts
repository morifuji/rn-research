import { createSlice, configureStore} from "@reduxjs/toolkit"
import { useSelector as rawUseSelector, TypedUseSelectorHook } from 'react-redux';


const counterSlice = createSlice({
    name: 'counter',
    initialState: 0,
    reducers: {
      increment: state => state + 1,
      decrement: state => state - 1
    }
  })

const store = configureStore({
  reducer: counterSlice.reducer
})

export const { increment, decrement } = counterSlice.actions

export default store

export type RootState = ReturnType<typeof store.getState>;

export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector
