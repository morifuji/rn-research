
import * as React from 'react';
import { View, Text, Button, TextInput, FlatList, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Top from "./src/Top"

import { Provider } from 'react-redux'
import { useSelector, increment } from "./src/store/index"
import store from "./src/store/index"

import AsyncStorage from '@react-native-community/async-storage';

const Tab = createBottomTabNavigator();

createBottomTabNavigator()
const Stack = createStackNavigator();

function Home() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Feed" component={() => <Text>Feed</Text>} />
      <Tab.Screen name="Messages" component={() => <Text>Message</Text>} />
    </Tab.Navigator>
  );
}

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text>Home Screen</Text>
    <Button
      title="Go to Details"
      onPress={() => navigation.navigate('DetailsScreen')}
    />
  </View>
    // <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    //   <Text>Home Screen</Text>
    // </View>
  );
}
function DetailsScreen() {
  const [text, setText] = React.useState("");

  const [hoge, huga] = React.useState("");

  const [imageList, setImageList] = React.useState<string[]>([]);

  const { count } = useSelector((state) => {
    return {
      count: state
    }
  })

  React.useEffect(() => {
    const f = () => {
      fetch('https://api.tenor.com/v1/trending?key=LIVDSRZULELA')
        .then((response) => {
          return response.json()
        })
        .then((json: {
          results: 
            {
              url: string
            }[]
        }) => {
          setImageList(json.results.map(v=>v.url))
        })
        .catch((error) => {
          console.error(error);
        });
    };
    f();
  }, []);

  React.useEffect(()=>{
    AsyncStorage.getItem('storage_Key').then(res=>{
      if (res===null) return
      setText(res)
    })
  }, [])

  React.useEffect(() => {
    console.log("HHOK", text)
    AsyncStorage.setItem('storage_Key', text)
  }, [text])

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      <TextInput
          style={{ height: 40 }}
          placeholder="Type here to translate!"
          onChangeText={input => setText(input)}
          value={text}
        />
        <Text style={{ padding: 10, fontSize: 42 }}>
          {text
            .split(' ')
            .map(word => word && '🍕')
            .join(' ')}
        </Text>
        <Button onPress={() => store.dispatch(increment())} 
        title="インクリメント"/>
        <Text style={{ padding: 10, fontSize: 42 }}>
          {new Array(count).fill(null).map(v=>'🎉')}
        </Text>
        <FlatList
        data={imageList}
        renderItem={({ item }) => <Image 
        style={{width: 50, height: 100}}
        source={{
          uri: "https://placekitten.com/200/300",
        }}
         />}
        keyExtractor={url => url}
      />
    </View>
  );
}

function App() {
  return (
    <Provider store={store}>
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="HomeScreen" component={HomeScreen} />
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Top" component={Top} />
        <Tab.Screen name="DetailsScreen" component={DetailsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
    </Provider>
  );
}

export default App;