## できたこと

- Helloworld
- ウィジェット配置
- webで起動確認
- Redux+ToolKitの検証。[こちら](https://future-architect.github.io/articles/20200501/) がとても参考になりました
- ローカルのストレージwebで動くか検証
- cssの当て方検証
- Tab.NavigatorとStack.Navigatorの調査

## TODO

- リッチなサンプルオブジェクトのコードリーディング
- ガッツリスタイリング
- lint
- CI
- CodePush
- ReactNativeFirebase
- [この](https://github.com/react-native-kit/react-native-track-player)MusicPlayerの検証
